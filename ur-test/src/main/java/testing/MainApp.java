package testing;

import java.io.PrintWriter;

import gov.nist.healthcare.unified.enums.Context;
import gov.nist.healthcare.unified.expressions.Action;
import gov.nist.healthcare.unified.expressions.Exp;
import gov.nist.healthcare.unified.filter.Condition;
import gov.nist.healthcare.unified.filter.Filter;
import gov.nist.healthcare.unified.filter.Restriction;
import gov.nist.healthcare.unified.model.EnhancedReport;
import gov.nist.healthcare.unified.model.impl.ModelImpl;
import gov.nist.healthcare.unified.proxy.ValidationProxy;
import validator.Util;

public class MainApp {
	public static void main(String[] args) {
		try {

			// Instanciate the Proxy
			ValidationProxy vp = new ValidationProxy(
					"Unified Report Test Application", "NIST", "1.0");

		
			// Perform a validation
			EnhancedReport report = vp.validate("/files/msgtt",
					"/files/VXU_P.xml",
					"/files/VXU_C.xml",
					"/files/VXU_V.xml",
					"aa72383a-7b48-46e5-a74a-82e019591fe7", Context.Free);

			
			//Create a Restriction
			Restriction rest1 = new Restriction();
			rest1.conditions.add(new Condition("category",Exp.Equals,"Constraint Success"));
			rest1.action = Action.KeepOne;
			
			//Create another Restriction
			Restriction rest2 = new Restriction();
			rest2.conditions.add(new Condition("path",Exp.Format,"RXA\\[[1-9]*\\]-9\\[2\\]"));
			rest2.conditions.add(new Condition("classification",Exp.Equals,"error"));
			rest2.action = Action.Remove;
			
			//Instanciate Filter
			Filter filter = new Filter();
			//Add restrictions
			filter.restrain(rest1);
			filter.restrain(rest2);
			
			//Filter report
			EnhancedReport filtered = filter.filter(report);
			
			//Get Report as JSON
			String JSON = filtered.to("json").toString();
			System.out.println(JSON);


		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
